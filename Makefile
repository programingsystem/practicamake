CC=gcc
IDIR =./includes
CFLAGS=-c -lm -I$(IDIR)



all: point


_DEPS = point.h
DEPS = $(patsubst %,$(IDIR)/%,$(_DEPS))

point: point.o main.o
	$(CC) -o point point.o main.o -lm -I$(IDIR)

point.o: point.c
	$(CC) $(CFLAGS) point.c


main.o: main.c $(DEPS)
	$(CC) $(CFLAGS) main.c

clean:
	rm *o point
