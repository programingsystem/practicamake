#include <point.h>

void distancia(Point p1, Point p2){
	
	
	float dist=sqrt((pow(p2.x-p1.x,2))+(pow(p2.y-p1.y,2))+(pow(p2.z-p1.z,2)));
	printf("\nLa distancia entre puntos es %f\n",dist);
	
}

void puntomedio(Point p1, Point p2){
	float puntoX =(p1.x + p2.x)/2;
	float puntoY=(p1.y + p2.y)/2;
	float puntoZ =(p1.z +p2.z)/2;
	
	printf("\nLa coordenada del punto medio es: (%f, %f, %f)\n",puntoX,puntoY,puntoZ); 
}
